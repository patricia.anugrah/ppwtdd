from django import forms

class SubscribeForm(forms.Form):
    name = forms.CharField(required=True, max_length=100, widget=forms.TextInput(attrs={'label': 'Name', 'placeholder': 'Full Name', 'id' : 'name'}))
    email = forms.CharField(required=True, max_length=100, widget=forms.TextInput(attrs={'label': 'Email', 'type': 'email', 'placeholder':'example@email.com', 'id' : 'email'}))
    password = forms.CharField(required=True, min_length=8, max_length=20, widget=forms.TextInput(attrs={'label': 'Password', 'type': 'password', 'placeholder': 'Password', 'id' : 'password'}))

