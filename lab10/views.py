from django.http import JsonResponse
from django.shortcuts import render
from .models import Subscriber
from .forms import SubscribeForm

response = {}
def lab10(request):
	return render(request,'lab10.html')

def subscribe(request):
    response['activetab'] = 'subscriberpage'
    if request.method == 'POST':
        form = SubscribeForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            status_subscribe = "data berhasil disimpan"

            try:
                Subscriber.objects.create(name=name, email=email, password=password)
            except:
                status_subscribe = "email tersebut sudah pernah didaftarkan sebelumnya, silakan daftar dengan email lain"

            return JsonResponse({'status_subscribe' : status_subscribe})

    Form = SubscribeForm()
    response['form'] = Form
    return render(request, 'subscribe.html', response)

def validate_email(request):
    if request.method == 'POST':
        email = request.POST['email']
        validasi = Subscriber.objects.filter(email=email).exists()
        return JsonResponse({'is_email' : validasi})
