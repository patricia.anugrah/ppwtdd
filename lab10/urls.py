from django.urls import path
from .views import *

urlpatterns = [
    path('', lab10, name='lab10'),
    path('subscribe', subscribe, name='subscribe'),
    path('validate_email/', validate_email, name = 'validate_email'),
]
