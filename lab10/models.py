from django.db import models

class Subscriber(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100, primary_key=True, unique=True)
    password = models.CharField(max_length=20)

