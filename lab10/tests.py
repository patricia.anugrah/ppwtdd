from django.test import TestCase, Client
from django.urls import resolve

from .views import lab10

class Lab10_Test(TestCase):

    def test_lab10_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab10_is_exist(self):
        response = Client().get('/lab10/')
        self.assertTemplateUsed(response, 'lab10.html')
    
    def test_subscribe_is_exist(self):
        response = Client().get('/lab10/subscribe')
        self.assertEqual(response.status_code, 200)
