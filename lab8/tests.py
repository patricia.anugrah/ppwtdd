from django.test import TestCase, Client
from django.urls import resolve

from .views import lab8

class Lab8_Test(TestCase):

    def test_lab_8_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab8_url_404_error(self):
        response = Client().get('/lab6/')
        self.assertEqual(response.status_code, 404)