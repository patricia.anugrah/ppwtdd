from django.urls import path
from .views import *

urlpatterns = [
    path('', lab9, name='lab9'),
    path('data/', data, name='books_data'),
]
