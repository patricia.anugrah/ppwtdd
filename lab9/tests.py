from django.test import TestCase, Client
from django.urls import resolve

from .views import lab9

class Lab9_Test(TestCase):

    def test_lab9_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_lab9(self):
        response = Client().get('/lab9/')
        self.assertTemplateUsed(response, 'lab9.html')
    
    def test_json_data_url_is_exist(self):
        response = Client().get('/lab9/data')
        self.assertEqual(response.status_code, 301)
