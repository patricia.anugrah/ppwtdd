from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests, json

def lab9(request):
	return render(request,'lab9.html')

def data(request):
    r = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting/')
    data = r.json()
    return JsonResponse(data)
