$("document").ready(function() {
    $("input").change(function() {
      if(this.checked){
          $('link[href="/static/style.css"]').attr('href', '/static/style.css');
          document.getElementById('night').innerHTML = '<strong>Good Night!</strong>';
        } else {
          $('link[href="/static/style2.css"]').attr('href', '/static/style2.css');
          document.getElementById('night').innerHTML = '<strong>Night Mode?</strong>';
        }
    });
    
    $(function () {
        $("#accordion").accordion({
          collapsible: true,
          active: false,
        });
    });
})
