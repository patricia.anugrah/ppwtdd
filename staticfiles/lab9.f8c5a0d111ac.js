$(document).ready(function() {
    setTimeout(function() {
        $('#loader').fadeOut('slow');
    }, 1500);
	$.ajax({
		url: 'data',
		datatype: 'json',
		success: function(data){
			var result ='<tr>';
			for(var i = 0; i < data.items.length; i++) {
				result += "<th class='align-middle text-center'>" + (i+1) + "</th>" +
				"<td><img style='width:20vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td class='align-middle' style='text-align:center'>" + data.items[i].volumeInfo.title +"</td>" +
				"<td class='align-middle' style='text-align:center'>" + data.items[i].volumeInfo.authors + "</td>" + 
				"<td class='align-middle' style='text-align:center'>" + data.items[i].volumeInfo.publisher +"</td>" + 
				"<td class='align-middle' style='text-align:center'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
				"<td class='align-middle' style='text-align:center'>" + "<img id='star" + i + "'onclick='addfav(this.id)' width='30px' src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSY1PVFE1qHFTDXo4ew8c4hvD-TpIhBLWwV6GShufI_ILshmE9w'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});

var countfav = 0;
function addfav(faved){
    var whitestar = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSY1PVFE1qHFTDXo4ew8c4hvD-TpIhBLWwV6GShufI_ILshmE9w'
    var blackstar = 'https://mbtskoudsalg.com/images/star-icon-png-1.png'
	var btn = document.getElementById(faved);
	if (btn.classList.contains("checked")) {
		btn.classList.remove("checked");
		document.getElementById(faved).src = whitestar;
		countfav--;
	}
	else {
		btn.classList.add('checked');
		document.getElementById(faved).src = blackstar;
		countfav++;
    }
    document.getElementById("counter").innerHTML = countfav;
}
