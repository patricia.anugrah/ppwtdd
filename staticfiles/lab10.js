$(document).ready(function () {

    $('#name').on('input', function () {
        var input = $(this);
        check(input);
    });

    var timer = null;
    $('#email').keydown(function () {
        var input = $("#email");
        check(input);
    });

    $('#password').on('input', function () {
        var input = $(this);
        check(input);
    });

    var check = function (input) {
        if (arr == 1) {
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var is_el = reg.test(input.val());
            if (is_el) {
                checkEmail(input.val());
                return
            } else {
                $(input).parent().removeClass('alert-validate2');
            }
        } else {
            var is_el = input.val();
        }
    };

    var checkEmail = function (email) {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $.ajax({
            method: "POST",
            url: "validate_email/",
            headers:{
                "X-CSRFToken": csrftoken
            },
            data: {email: email},
            success: function (response) {
                var inptEmail = $("#email");
                if (response.is_email) {
                    inptEmail.parent().addClass('alert-validate2');
                } else {
                    inptEmail.parent().removeClass('alert-validate2');
                }
            },
        })
    };

    $(function () {
        var csrftoken = $("[name=csrfmiddlewaretoken]").val();
        $('form').on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: '',
                headers:{
                    "X-CSRFToken": csrftoken
                },
                data: $('form').serialize(),
                success: function(status) {
                    if (status.status_subscribe === "data berhasil disimpan") {
                        var title1 = "<p style='font-size: 22px'><strong>Thank you for subscribing!</strong></p>";
                        $("form").remove();
                    } 
                    $("title_s").replaceWith(title1);
                    }
                    $('#name').val("");
                    $('#email').val("");
                    $('#password').val("");
                },
            });
        });
    });
});
