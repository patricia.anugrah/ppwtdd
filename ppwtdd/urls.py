"""ppwtdd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.urls import re_path
from django.conf.urls import include
from lab6.views import home
import lab6.urls as lab6
from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from django.conf import settings

urlpatterns = [
	re_path(r'^admin/', admin.site.urls),
    #path('admin/', admin.site.urls),
	#path('home/'), home, name="home"),
    path('lab8/', include('lab8.urls')),
    path('lab9/', include('lab9.urls')),
    path('lab10/', include('lab10.urls')),
    path('lab10/subscribe', include('lab10.urls')),
	re_path(r'^', include(lab6)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
