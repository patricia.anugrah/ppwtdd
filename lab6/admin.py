from django.contrib import admin

# Register your models here.
from .models import StatusModel
admin.site.register(StatusModel)
