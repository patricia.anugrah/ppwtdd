from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import StatusModel

response = {}
def home(request):
	form = StatusForm(request.POST or None)
	if(request.method == "POST" and form.is_valid()):
		if (form.is_valid()):
			status = request.POST.get("status")
			date = request.POST.get("date")
			StatusModel.objects.create(status=status, date=date)
			return HttpResponseRedirect('/')
		else:
			return render(request, 'status.html', response)
	else:
		allStatus = StatusModel.objects.all()
		response['form'] = form
		response['allStatus'] = allStatus
		return render(request, 'status.html', response)
