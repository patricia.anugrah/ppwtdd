from django.test import TestCase, Client
from django.urls import resolve

from .forms import StatusForm
from .models import StatusModel
from .views import home

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time


class Lab6_Test(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_url_404_error(self):
        response = Client().get('/lab6/')
        self.assertEqual(response.status_code, 404)

    def test_form_validation_for_blank_field(self):
        form = StatusForm(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['status'], ["This field is required."])

    def test_lab6_has_status_input(self):
        form = StatusForm()
        self.assertIn('id="id_status"', form.as_p())

'''
class Lab7_FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        # ---------------------------------------------------#
        # Untuk chrome tidak membuka localhost secara otomatis
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # ---------------------------------------------------#
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab7_FunctionalTest, self).setUp()

    def test_input_todo(self):
        selenium = self.selenium
        # Untuk membuka localhost
        selenium.get('http://127.0.0.1:8000/')
        # Untuk mencari form yang ingin diisi, yaitu status dan button-nya
        input_status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_class_name('button')
        # Untuk mengisi status dengan kata "Coba Coba"
        input_status.send_keys('Coba Coba')
        # Untuk submit form status-nya
        submit.send_keys(Keys.RETURN)
        # Untuk user dapat melihat proses bekerjanya ^^
        time.sleep(5)
        # cek status ada di webpage-nya
        self.assertIn('Coba Coba', selenium.page_source)


    def tearDown(self):
        self.selenium.quit()
        super(Lab7_FunctionalTest, self).tearDown()

    def test_font_style(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        font_style = selenium.find_element_by_tag_name('body').value_of_css_property('font-family')
        self.assertEqual(font_style, '"Century Gothic", sans-serif')

    def test_h1_color(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        color = selenium.find_element_by_tag_name('h1').value_of_css_property('color')
        self.assertEqual(color, 'rgba(255, 255, 255, 1)')

    def test_h1_title(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        h1_title = selenium.find_element_by_tag_name('h1').text
        self.assertEqual(h1_title, 'Hello, Apa kabar?')

    def test_submit_button_location_is_exist(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        location_submit_button = selenium.find_element_by_tag_name('button')
 '''   