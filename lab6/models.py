from django.db import models
from django.utils import timezone

class StatusModel(models.Model):
	status = models.CharField(max_length=300)
	date = models.DateTimeField(auto_now_add=True)
