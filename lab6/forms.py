from django import forms
from .models import StatusModel
title_attrs = {
     'type': 'text',
     'class': 'status-form-input',
     'id': 'id_status'
 }

class StatusForm(forms.Form):
	status = forms.CharField()
